# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs cmake flag-o-matic #python-single-r1

DESCRIPTION="JasPer Image Processing/Coding Tool Kit"
HOMEPAGE="http://www.ece.uvic.ca/~mdadams/jasper"
SRC_URI="https://github.com/jasper-software/jasper/archive/refs/tags/version-2.0.33.tar.gz -> jasper-2.0.33.tar.gz"


LICENSE="JasPer-2.0"
SLOT="0"
KEYWORDS="~arm64"
IUSE="heif heic asan lsan msan tsan ubsan mif jpc threads nptl msvc glut"

RDEPEND="
	heif? ( media-libs/libheif )

"
DEPEND="${RDEPEND}
	glut? ( media-libs/freeglut )"

BDEPEND="
	dev-util/cmake
	"

src_unpack() {
	default
	mv jasper-version-2.0.33 jasper-2.0.33/
}

src_configure() {
	local mycmakeargs=(
		-DJAS_ENABLE_AUTOMATIC_DEPENDENCIES=OFF
		-DJAS_ENABLE_ASAN=$(usex asan)
		-DJAS_ENABLE_LSAN=$(usex lsan)
		-DJAS_ENABLE_MSAN=$(usex msan)
		-DJAS_INCLUDE_JPC_CODEC=$(usex jpc)
		-DJAS_ENABLE_OPENGL=$(usex glut)
	)


	cmake_src_configure
}

