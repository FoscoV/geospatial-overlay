# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs cmake flag-o-matic #python-single-r1

DESCRIPTION="JasPer Image Processing/Coding Tool Kit"
HOMEPAGE="http://www.ece.uvic.ca/~mdadams/jasper"
SRC_URI="https://github.com/jasper-software/jasper/archive/refs/tags/version-3.0.3.tar.gz -> jasper-3.0.3.tar.gz"


LICENSE="JasPer-2.0"
SLOT="0"
KEYWORDS="~arm64"
IUSE="heif heic asan lsan msan tsan ubsan mif jpc threads nptl msvc"

RDEPEND="
	heif? ( media-libs/libheif )

"
DEPEND="${RDEPEND}"

BDEPEND="
	dev-util/cmake
	"

src_unpack() {
	default
	mv jasper-version-3.0.3 jasper-3.0.3
}

src_configure() {
	local mycmakeargs=(
		-DJAS_STDC_VERSION=201112L
		-DJAS_CROSSCOMPILING=OFF
		-DJAS_ENABLE_MULTITHREADING_SUPP=$(usex threads)
		-DJAS_PREFER_PTHREAD=$(usex nptl)
		-DJAS_HAVE_MSVC_FSANITIZE_ADDRESS=$(usex msvc)
		-DJAS_ENABLE_ASAN=$(usex asan)
		-DJAS_ENABLE_LSAN=$(usex lsan)
		-DJAS_ENABLE_MSAN=$(usex msan)
		-DJAS_ENABLE_TSAN=$(usex tsan)
		-DJAS_ENABLE_UBSAN=$(usex ubsan)
		-DJAS_ENABLE_HEIC_CODEC=$(usex heic)
		-DJAS_ENABLE_MIF_CODEC=$(usex mif)
		-DJAS_INCLUDE_JPC_CODEC=$(usex jpc)
	)


	cmake_src_configure
}

