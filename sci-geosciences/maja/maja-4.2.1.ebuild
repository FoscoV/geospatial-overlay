# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs cmake

DESCRIPTION="atmospheric correction and cloud screening software based on the MACCS-Atcor processor"
HOMEPAGE="https://github.com/CNES/MAJA"
SRC_URI="https://github.com/CNES/MAJA/archive/${PV}.tar.gz -> maja-${PV}.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~arm64"
IUSE="tu tv tva test"

BDEPEND="
	>=dev-util/cmake-3.4
	sci-libs/hdf
	sci-libs/netcdf
"


src_unpack() {
	default
        mv MAJA-${PV} maja-${PV}
}


src_configure() {
	local mycmakeargs=(
			-DENABLE_TU=$(usex tu)
			-DENABLE_TV=$(usex tv)
			-DENABLE_TVA=$(usex tva)
			-DBUILD_TESTING=$(usex test)
			-DCMAKE_INSTALL_PREFIX="/usr"
		)

}
