
# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 cmake flag-o-matic toolchain-funcs
DESCRIPTION="Linux kernel driver for the battery health control WMI interface of Acer laptops"

HOMEPAGE="https://github.com/OpenDroneMap/dem2mesh"

EGIT_REPO_URI="https://github.com/OpenDroneMap/dem2mesh.git"


LICENSE="GPL-3"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""



BDEPEND=" sys-devel/gcc[openmp] 
sci-libs/gdal
"


BUILD_TARGETS="clean all"
