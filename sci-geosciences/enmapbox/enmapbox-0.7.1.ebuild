# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
PYTHON_COMPAT=( pypy python3_{7,8,9,10,11} )
inherit toolchain-funcs python-r1
DESCRIPTION="Transitional to dependencies in QGIS Plugin to visualize and process remote sensing raster data. It is particularly developed to handle imaging spectroscopy data, as from the upcoming EnMAP sensor."
HOMEPAGE="https://www.enmap.org/data_tools/enmapbox/"
SRC_URI="https://git.gfz-potsdam.de/EnMAP/GFZ_Tools_EnMAP_BOX/enpt_enmapboxapp/-/archive/v0.7.1/enpt_enmapboxapp-v0.7.1.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~arm64"
IUSE="astropy cuda"

RDEPEND="
        sci-geosciences/qgis
        >=dev-python/scipy-1.1[${PYTHON_USEDEP}]
        >=sci-libs/scikit-learn-0.24
	>=dev-python/numpy-1.23[${PYTHON_USEDEP}]
	>=dev-python/matplotlib-3.0
	dev-python/pyopengl
	dev-python/xgboost[${PYTHON_USEDEP}]
	cuda? ( dev-python/catboost )
        dev-python/h5py
        dev-python/numba
        || (
		dev-python/netcdf4-python
		dev-python/netcdf4
	)
	astropy? ( dev-python/astropy )
        "
#        <=dev-python/numba-0.47 give to the updated version a try, leaving it here ... just in case
#	dev-python/lightgbm
#	dev-python/numba-scipy


DEPEND="${RDEPEND}"
BDEPEND=""

src_unpack(){
	default
	mv enpt_enmapboxapp-v0.7.1 enmapbox-0.7.1
}

src_prepare(){
echo '
[install]
prefix={D}
'>> setup.cfg

default
}

src_compile(){
	default
	python scrpts/create_plugin.py
}

pkg_postinst(){
	ewarn "This atom is intended to manage runtime dependencies"
	ewarn "The processing core of the declared app is in the corresponding Qgis plugin"
	ewarn "Find more here:"
	ewarn "https://enmap-box.readthedocs.io/en/latest/usr_section/usr_installation.html#install-from-qgis-plugin-repository"
}


