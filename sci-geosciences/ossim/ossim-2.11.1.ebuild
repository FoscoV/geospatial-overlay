# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit cmake


MY_PV="OrchidIsland-${PV}"
MY_P="${P}-${MY_PV}"


DESCRIPTION="A powerful suite of geospatial libraries and applications used to process imagery, maps, terrain, and vector data"
HOMEPAGE="https://trac.osgeo.org/ossim/"
SRC_URI="https://github.com/ossimlabs/ossim/archive/OrchidIsland-${PV}.zip"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm64"
IUSE="hdf5 planet png jpeg opencv gdal fftw"

DEPEND="opencv? ( media-libs/opencv )
	hdf5? ( sci-libs/hdf5 )
	gdal? ( sci-libs/gdal )
	fftw? ( sci-libs/fftw )
	png? ( media-libs/libpng )"
RDEPEND="${DEPEND}"
BDEPEND="sci-libs/geos
	media-libs/freetype
	sys-libs/glibc
	media-libs/libjpeg-turbo
	sys-libs/zlib
	dev-util/cmake
	app-arch/unzip"


src_unpack(){
	default
	mv ossim-OrchidIsland-2.11.1 ${P}
}

src_prepare() {
	OSSIM_INSTALL_PREFIX="/usr/bin"
	export OSSIM_INSTALL_PREFIX

	sed -i 's/QT_BINARY_DIR=\"\/usr\/local\/bin\\"/QT_BINARY_DIR=\"\/usr\/bin\\"/g' ./cmake/scripts/ossim-cmake-config.sh
	if use hdf5; then
		sed -i 's/BUILD_OSSIM_HDF5_SUPPORT=OFF/BUILD_OSSIM_HDF5_SUPPORT=ON/g' ./cmake/scripts/ossim-cmake-config.sh
		sed -i 's/BUILD_HDF5_PLUGIN=OFF/BUILD_HDF5_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	if use planet; then
		sed -i 's/BUILD_OSSIM_PLANET=OFF/BUILD_OSSIM_PLANET=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	if use png; then
		sed -i 's/BUILD_PNG_PLUGIN=OFF/BUILD_PNG_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	if use jpeg; then
		sed -i 's/BUILD_OPENJPEG_PLUGIN=OFF/BUILD_OPENJPEG_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
		sed -i 's/BUILD_JPEG12_PLUGIN=OFF/BUILD_JPEG12_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	if use opencv; then
		sed -i 's/BUILD_OPENCV_PLUGIN=OFF/BUILD_OPENCV_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	if use gdal; then
		sed -i 's/BUILD_GDAL_PLUGIN=OFF/BUILD_GDAL_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	if use fftw; then
		sed -i 's/BUILD_FFTW3_PLUGIN=OFF/BUILD_FFTW3_PLUGIN=ON/g' ./cmake/scripts/ossim-cmake-config.sh
	fi
	cmake_src_prepare
}

