# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs cmake

DESCRIPTION="SAR Interferometric processing chain based on Orfeo ToolBox (port from Diapason)"
HOMEPAGE="https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/"
SRC_URI="https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/archive/1.1.0_beta_4/diapotb-1.1.0_beta_4.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	dev-python/jsonschema
	dev-libs/jsoncpp
	sci-libs/gdal
	sci-geosciences/orfeotb[python]
	dev-python/h5py
	"
BDEPEND="sci-geosciences/orfeotb"

PATCHES=(
#	"${FILESDIR}"/${P}-syntaxVectorDclaration.patch
)


src_unpack() {
	default
	mv diapotb-1.1.0_beta_4 diapotb-1.1.0
}
src_configure() {
	export OTB_DIR=/usr/lib/cmake/OTB-7.4
	export LD_LIBRARY_PATH=/usr/lib/otb/lib:$ LD_LIBRARY_PATH

	local mycmakeargs=(
		-DOTB_BUILD_MODULE_AS_STANDALONE=ON
		-DCMAKE_INSTALL_PREFIX=/usr/lib/otb/
		)
	cmake_src_configure
}
