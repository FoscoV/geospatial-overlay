# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs cmake

DESCRIPTION="SAR Interferometric processing chain based on Orfeo ToolBox (port from Diapason)"
HOMEPAGE="https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/"
SRC_URI="https://gitlab.orfeo-toolbox.org/remote_modules/diapotb/-/archive/d84b9accbba81d966c51410dee32c866295594b1/diapotb-d84b9accbba81d966c51410dee32c866295594b1.tar.gz -> diapotb-2.0.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64 ~x86 ~arm64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}
	dev-python/jsonschema
	sci-libs/gdal"
BDEPEND="sci-geosciences/orfeotb"

PATCHES=(
#	"${FILESDIR}"/${P}-syntaxVectorDclaration.patch
)

src_unpack() {
	default
	mv diapotb-468af1fa65134b57940b406ddba4c2f104acf7d8 diapotb-2.0
}

src_configure() {
        export OTB_DIR=/usr/lib/cmake/OTB-8.0
        export LD_LIBRARY_PATH=/usr/lib:$LD_LIBRARY_PATH

        local mycmakeargs=(
                -DOTB_BUILD_MODULE_AS_STANDALONE=ON
                -DCMAKE_INSTALL_PREFIX=/usr/
                )
        cmake_src_configure
}

