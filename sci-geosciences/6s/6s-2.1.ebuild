# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION=""
HOMEPAGE="https://salsa.umd.edu/index.html"
SRC_URI="https://salsa.umd.edu/files/6S/6SV2.1.tar"

LICENSE="TODO"
SLOT="0"
KEYWORDS="amd64 ~x86 ~arm64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_prepare(){
	echo "pure code, nothing to prepare"
}
src_configure(){
	echo "pure code, nothing to configure"

}
