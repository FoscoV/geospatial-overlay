# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit toolchain-funcs cmake
MY_PN="otb"
MY_PV="${MY_PN}-${PV}"
DESCRIPTION="State-of-the-art remote sensing. It can process high resolution optical, multispectral and radar images at the terabyte scale"
HOMEPAGE="https://www.orfeo-toolbox.org/"
SRC_URI="https://gitlab.orfeo-toolbox.org/orfeotoolbox/otb/-/archive/${PV}/${MY_PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~arm64"
IUSE="6s curl glew glfw glut svm mpi muparser opencv opengl qt qwt shark sift sptw examples sse"

RDEPEND="
	=sci-libs/itk-4.13.3
	>=sci-libs/gdal-2.4.1[jpeg,jpeg2k]
	>=sci-geosciences/ossim-1.8.20
	media-libs/tiff
	dev-libs/boost
	sci-libs/geos
	dev-games/openscenegraph
	dev-libs/tinyxml
	muparser? ( dev-cpp/muParser )
	curl? ( net-misc/curl )
	sci-libs/fftw:3.0
	glew? ( media-libs/glew )
	glfw? ( >=media-libs/glfw-3.0 )
	glut? ( dev-games/openscenegraph[glut] )
	svm? ( sci-libs/libsvm )
	shark? ( sci-libs/shark )
	mpi? ( virtual/mpi )
	"
#	klm? ( ) iuse is not yet supported
DEPEND="${RDEPEND}"
BDEPEND=""

src_unpack() {
	default
        mv otb-7.2.0 orfeotb-7.2.0
}

src_configure() {
	local mycmakeargs=(
			-DOTB_USE_CURL=$(usex curl)
			-DOTB_USE_GLFW=$(usex glfw)
			-DOTB_USE_6S=$(usex 6s)
			-DOTB_USE_GLUT=$(usex glut)
			-DOTB_USE_GLEW=$(usex glew)
			-DOTB_USE_MPI=$(usex mpi)
			-DOTB_USE_MUPARSER=$(usex muparser)
			-DOTB_USE_OPENCV=$(usex opencv)
			-DOTB_USE_OPENGL=$(usex opengl)
			-DOTB_USE_SHARK=$(usex shark)
			-DOTB_USE_LIBSVM=$(usex svm)
			-DOTB_USE_SSE_FLAGS=$(usex sse)
			-DOTB_USE_SIFTFAST=$(usex sift)
			-DOTB_USE_QT=$(usex qt)
			-DBUILD_EXAMPLES=$(usex examples)
	)

	cmake_src_configure
}