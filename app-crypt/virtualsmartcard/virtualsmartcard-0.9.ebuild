# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{11,12} pypy3 )
inherit python-single-r1 autotools


DESCRIPTION="Emulation of different types of smart card readers or smart cards themselves"

HOMEPAGE="https://frankmorgner.github.io/vsmartcard/index.html"

SRC_URI="https://github.com/frankmorgner/vsmartcard/releases/download/${P}/${P}.tar.gz"

LICENSE="GPL"

SLOT="0"
KEYWORDS="amd64"
IUSE=""

RDEPEND="sys-apps/pcsc-lite
media-gfx/qrencode"

src_configure() {
	rm $WORKDIR/$P/py-compile
	autoupdate
	eautoreconf --verbose --install
	$WORKDIR/$P/configure --prefix="" --exec-prefix=/usr --sysconfdir=/etc --mandir=/usr/share/man
}

