# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( pypy python3_{7,8,9,10,11} )

DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="Python module for processing hyperspectral image data"
HOMEPAGE="https://github.com/spectralpython/spectral
	http://www.spectralpython.net/"
SRC_URI="https://github.com/spectralpython/spectral/archive/refs/tags/${PV}.tar.gz -> ${PF}.tar.gz"


LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 arm64"
IUSE=""



RDEPEND="dev-python/numpy
	dev-python/wxpython
	dev-python/matplotlib
	dev-python/ipython
	dev-python/pyopengl
"
DEPEND=""
