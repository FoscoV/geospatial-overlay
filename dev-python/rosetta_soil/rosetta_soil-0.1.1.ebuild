# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 pypi

DESCRIPTION="Implementation of the Rosetta pedotransfer function model for predicting unsaturated soil hydraulic parameters"
HOMEPAGE="https://github.com/usda-ars-ussl/rosetta-soil"
SRC_URI="https://files.pythonhosted.org/packages/47/7b/d2b7c5f8e13de10725d88672f51e8a7e14e561ee22d0efdab0dc791d1dda/rosetta-soil-0.1.1.tar.gz -> ${P}.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64 ~x86"

RDEPEND="
	dev-python/numpy
"

src_unpack() {
	default
	mv ${WORKDIR}/rosetta-soil-0.1.1 ${WORKDIR}/rosetta_soil-0.1.1
}
