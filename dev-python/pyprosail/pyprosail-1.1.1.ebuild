# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( pypy python3_{7,8,9,10,11,12} )

DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="Python interface to the ProSAIL combined leaf and canopy optical model"
HOMEPAGE="https://github.com/EOA-team/PyProSAIL
	https://pypi.org/project/pyprosail/"
SRC_URI="https://files.pythonhosted.org/packages/0d/84/1537f31a8608f351a4ecedda3fc6cbf49f4d251d15901ab7d2f1c4b41055/pyprosail-1.1.1.tar.gz -> ${PF}.tar.gz"


LICENSE="LGPL"
SLOT="0"
KEYWORDS="amd64 arm64"
IUSE=""



RDEPEND="dev-python/scipy
	dev-python/numpy
"
DEPEND=""

