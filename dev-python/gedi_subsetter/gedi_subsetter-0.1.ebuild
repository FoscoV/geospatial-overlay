# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( pypy python2_7 python3_{3,4,5,6,7,8,9,10,11,12} )

inherit toolchain-funcs python-r1

DESCRIPTION="Data Prep Script for subsetting GEDI granules spatially and by band/layer, and exporting subsets as GeoJSON files."
HOMEPAGE="https://git.earthdata.nasa.gov/projects/LPDUR/repos/gedi-subsetter"
SRC_URI="https://git.earthdata.nasa.gov/projects/LPDUR/repos/gedi-subsetter/raw/GEDI_Subsetter.py?at=refs%2Fheads%2Fmain -> gedi_subsetter.py"


LICENSE="TODO"
SLOT="0"
KEYWORDS="~arm64"
IUSE=""



RDEPEND="dev-python/h5py
	sci-libs/shapely
	dev-python/geopandas
	dev-python/pandas
	sci-libs/rtree
"
DEPEND=""

src_unpack(){

mkdir ${S}

cp ${DISTDIR}/${A} ${S}/${PN}.py
#echo "nothing to be done"
}

src_prepare(){
echo "nothing to be done"
eapply_user
}
src_configure(){
echo "nothing to be done"
}
src_compile(){
echo "nothing to be done"
}
src_install(){
	mkdir -p ${D}/usr/bin/
	cp "${S}/${PN}.py" "${D}/usr/bin/" || die "Install failed!"
	chmod +x ${D}/usr/bin/${PN}.py
}

