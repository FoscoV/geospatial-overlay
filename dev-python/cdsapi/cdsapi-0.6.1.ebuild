# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( pypy python3_{7,8,9,10,11,12} )

DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1

DESCRIPTION="The Climate Data Store (CDS) Application Program Interface (API) is a service providing programmatic access to CDS data."
HOMEPAGE="https://github.com/ecmwf/cdsapi"
SRC_URI="https://codeload.github.com/ecmwf/cdsapi/tar.gz/refs/tags/${PV} -> ${PF}.tar.gz"


LICENSE="Apache2.0"
SLOT="0"
KEYWORDS="amd64 arm64"
IUSE=""



RDEPEND=">=dev-python/requests-2.5.0
	dev-python/tqdm
"
DEPEND=""

pkg_postinst() {
elog "Get your user ID (UID) and API key from the CDS portal at the address https://cds.climate.copernicus.eu/user and write it into the configuration file, so it looks like:

\$ cat ~/.cdsapirc
url: https://cds.climate.copernicus.eu/api/v2
key: <UID>:<API key>
verify: 0


Remember to agree to the Terms and Conditions of every dataset that you intend to download.
"
}
