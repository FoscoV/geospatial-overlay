# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( pypy python3_{7,8,9,10,11,12} )

DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 pypi

DESCRIPTION=""
HOMEPAGE="https://pypi.org/project/DSSATTools/#files"
SRC_URI="https://files.pythonhosted.org/packages/1e/ae/149d22821aa19770345a7a067696aec21a7112f438c8d3883140b91d5eb4/DSSATTools-2.0.3.tar.gz"

LICENSE="GPL"
SLOT="0"
KEYWORDS="amd64 arm64"
IUSE=""



RDEPEND="
>=dev-python/attrs-21.4.0
>=dev-python/certifi-2021.10.8
>=dev-python/chardet-4.0.0
>=dev-python/charset-normalizer-2.0.9
>=dev-python/coverage-6.3.2
>=dev-python/fortranformat-1.2.2
>=dev-python/idna-3.3
>=dev-python/iniconfig-1.1.1
dev-python/numpy
>=dev-python/packaging-21.3
dev-python/pandas
>=dev-python/pluggy-1.0.0
>=dev-python/py-1.11.0
>=dev-python/pyparsing-3.0.7
>=dev-python/pytest-7.0.1
>=dev-python/pytest-cov-2.12.1
>=dev-python/python-dateutil-2.8.2
>=dev-python/pytz-2021.3
dev-python/requests
>=dev-python/rosetta_soil-0.1.1
>=dev-python/six-1.16.0
>=dev-python/tomli-2.0.1
dev-python/tomli-w
dev-python/tomlkit
sci-libs/dssat
"
DEPEND=""

src_unpack() {
	default
	mv ${WORKDIR}/DSSAT* ${WORKDIR}/dssattools-${PV}
	# this should/may be a patch but...
	touch ${WORKDIR}/dssattools-${PV}/requirements.txt
}

