# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit git-r3 linux-mod flag-o-matic toolchain-funcs
DESCRIPTION="Linux kernel driver for the battery health control WMI interface of Acer laptops"

HOMEPAGE="https://github.com/frederik-h/acer-wmi-battery"

EGIT_REPO_URI="https://github.com/frederik-h/acer-wmi-battery.git"


LICENSE="GPL-2"

SLOT="0"

KEYWORDS="~amd64"

IUSE=""



BDEPEND="sys-kernel/gentoo-sources"

MODULE_NAMES="acer-wmi-battery(acer_wmi_battery:${S}:${S})"

BUILD_TARGETS="clean all"

#pkg_setup() {
#	linux-mod_pkg_setup
#}

#src_compile() {
#	linux-mod_src_compile
#}

#src_install() {
#	linux-mod_src_install
#}

elog "acer-wmi-battery module is disabled by default, consider"
elog "modprobe acer-wmi-battery enable_health_mode=1"
elog "or add the line:"
elog "acer-wmi-battery enable_health_mode=1"
elog "into /etc/modules-load.d/"
elog "Further reading at https://github.com/frederik-h/acer-wmi-battery"
