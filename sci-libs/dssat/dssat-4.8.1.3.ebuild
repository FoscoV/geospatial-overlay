EAPI=7

inherit toolchain-funcs cmake
 
DESCRIPTION="The DSSAT Cropping System Model"
HOMEPAGE="The Decision Support System for Agrotechnology Transfer (DSSAT) Version"
SRC_URI="https://github.com/DSSAT/dssat-csm-os/archive/refs/tags/v4.8.1.3.tar.gz -> ${P}.tar.gz"
 
LICENSE="BSD-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
 
DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

CMAKE_BUILD_TYPE="RELEASE"

src_unpack(){
default
mv dssat-* ${P}
}

BUILD_DIR="${WORKDIR}/${P}/build"

src_configure(){
local mycmakeargs=(
	 -DCMAKE_INSTALL_PREFIX=/usr/bin/
)
cmake_src_configure
}
