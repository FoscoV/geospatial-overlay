# Copyright 1999-2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake
MY_PN="otb"
MY_PV="${MY_PN}-${PV}"
DESCRIPTION="State-of-the-art remote sensing. It can process high resolution optical, multispectral and radar images at the terabyte scale"
HOMEPAGE="https://www.orfeo-toolbox.org/"
SRC_URI="https://github.com/Shark-ML/Shark/archive/67990bcd2c4a90a27be97d933b3740931e9da141.zip -> shark-4.0.zip"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64 ~ppc ~x86 ~arm64"
IUSE="openmp"

RDEPEND="
	dev-libs/boost
"
DEPEND="${RDEPEND}"
BDEPEND=""




src_unpack() {
	default
        mv Shark-67990bcd2c4a90a27be97d933b3740931e9da141 shark-4.0
}


src_configure() {

	local mycmakeargs=(
		-DBUILD_DOCS:BOOL=OFF
		-DBUILD_EXAMPLES:BOOL=OFF
		-DBUILD_TESTING:BOOL=OFF
		-DENABLE_HDF5:BOOL=
		-DENABLE_CBLAS:BOOL=OFF
		-DENABLE_OPENMP:BOOL=${OTB_USE_OPENMP}
	)


	cmake_src_configure
}


