# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( pypy python3_{7,8,9,10,11,12} )

inherit distutils-r1

DESCRIPTION="Python interface to the ProSAIL combined leaf and canopy optical model"
HOMEPAGE="https://github.com/guofei9987/scikit-opt/
	https://pypi.org/project/scikit-opt"
SRC_URI="https://github.com/guofei9987/scikit-opt/archive/refs/tags/v0.6.5.tar.gz -> ${PF}.tar.gz"


LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 arm64"
IUSE=""



RDEPEND="dev-python/scipy
	dev-python/numpy
	dev-python/matplotlib
	dev-python/pandas
"
DEPEND=""

