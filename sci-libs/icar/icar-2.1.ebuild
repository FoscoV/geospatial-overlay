# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

FORTRAN_STANDARD="90"
inherit toolchain-funcs  fortran-2

DESCRIPTION="The Intermediate Complexity Atmospheric Research Model"
HOMEPAGE="https://github.com/NCAR/icar"
SRC_URI="https://github.com/NCAR/icar/archive/refs/tags/2.1.tar.gz -> icar-2.1.tar.gz"


LICENSE="MIT"
SLOT="0"
KEYWORDS="~arm64"
IUSE="openmp"

BDEPEND="dev-lang/cfortran"
RDEPEND="sci-libs/fftw[fortran]
        sci-libs/netcdf-fortran
	sys-cluster/opencoarrays
"
DEPEND="${RDEPEND}"


S="${WORKDIR}/${P}/src"


src_configure(){
	mf=`echo -n 'ECHO_MOVE=$(which echo)
        RM=$(which rm)
        CP=$(which cp)
        # doxygen only required to enable "make doc"
        DOXYGEN=doxygen
        FFTW=/usr
        NETCDF=/usr
        COMPILER=gnu
        INSTALLDIR=$D/usr/bin
        ' ; cat $S/makefile`
	echo "${mf}" > $S/makefile
	#fix make script from march=native to cflags ldflags and cppflags
	sed -i 's/-march=native/${CFLAGS}/g' $S/makefile
	sed -i 's/ -mfma / /g' $S/makefile
}

src_compile(){
	emake DESTDIR="${D}/usr/bin" install MODE=fast
}
