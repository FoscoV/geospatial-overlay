# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7


inherit toolchain-funcs  #python-single-r1

DESCRIPTION="NLM Insight Segmentation and Registration Toolkit"
HOMEPAGE="https://web.stanford.edu/group/radar/softwareandlinks/sw/snaphu/"
SRC_URI="https://web.stanford.edu/group/radar/softwareandlinks/sw/snaphu/snaphu-v2.0.4.tar.gz -> snaphu-2.0.4.tar.gz"


LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~arm64"
IUSE=""

RDEPEND=""
DEPEND="${RDEPEND}"


S="${WORKDIR}/${P}/src"
src_unpack() {
        default
        mv snaphu-v2.0.4 snaphu-2.0.4
}


src_configure() {
#	cd src
	sed -i -e 's:CC.*=.*::' Makefile 
	sed -i -e 's:CFLAGS.*=.*::' Makefile 
	sed -i -e 's:/usr/local:${D}:' Makefile
	sed -i -e 's:${D}/bin:${D}/bin/:' Makefile
	sed -i -e 's:$(CFLAGS):$(CPPFLAGS) $(CFLAGS) $(LDFLAGS):' Makefile
#	sed -i -e 's:cp $(SNAPHUMAN) $(MANDIR)/man1/:mkdir -p $(MANDIR)&&cp $(SNAPHUMAN) $(MANDIR)/:' Makefile

}

src_install() {
	mkdir -p ${D}/man/man1
	mkdir -p ${D}/bin/
	default
	mkdir -p ${D}/etc/snaphu
	cp ${WORKDIR}/${PF}/config/* ${D}/etc/snaphu/
	mkdir -p ${D}/usr/local/share/man

	mv ${D}/bin ${D}/usr/
	mv ${D}/man/man1 ${D}/usr/local/share/man/
}
