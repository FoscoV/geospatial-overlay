# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="The non interactive Sentinels product retriever from the Sentinels Data Hubs"
HOMEPAGE="https://github.com/SentinelDataHub/Scripts/blob/master/dhusget_readme_0.3.8.md"
SRC_URI="https://github.com/SentinelDataHub/Scripts/raw/master/dhusget_0.3.8.sh -> dhusget-0.3.8.sh"


LICENSE="TODO"
SLOT="0"
KEYWORDS="arm64 amd64"
IUSE=""

RDEPEND="
	net-misc/wget
"

DEPEND="${RDEPEND}"

src_unpack() {
	cp ../distdir/dhusget-0.3.8.sh ./
	mkdir ${S}
	cp ./dh*sh ${S}/
	echo "shell script already extracted, copied"
}

src_install() {
	mkdir -p ${D}/usr/bin
	cp ${S}/dhus*sh ${D}/usr/bin/dhusget
}


pkg_preinst() {
	fperms +x /usr/bin/dhusget
}
