# Copyright 1999-2017 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7


inherit toolchain-funcs cmake #python-single-r1

DESCRIPTION="NLM Insight Segmentation and Registration Toolkit"
HOMEPAGE="http://www.itk.org"
SRC_URI="https://github.com/InsightSoftwareConsortium/ITK/archive/v4.13.3.tar.gz -> itk-4.13.3.tar.gz"


LICENSE="Apache-2.0"
SLOT="4"
KEYWORDS="~arm64 ~amd64"
IUSE=""

RDEPEND="
	dev-libs/double-conversion:0=
	media-libs/libpng:0=
	media-libs/tiff:0=
	sci-libs/dcmtk:0=
	sci-libs/hdf5:0=[cxx]
	sys-libs/zlib:0=
	virtual/jpeg:0=
	dev-libs/expat
	sci-libs/fftw:3.0=
	<sci-libs/vtk-9.0[gdal,imaging,boost,offscreen]
"
DEPEND="${RDEPEND}"


src_unpack() {
	default
	mv ITK-4.13.3 itk-4.13.3
}

src_configure() {
	local mycmakeargs=(
		-DModule_ITKCommon:BOOL=ON
		-DModule_ITKFiniteDifference:BOOL=ON
		-DModule_ITKGPUCommon:BOOL=ON
		-DModule_ITKGPUFiniteDifference:BOOL=ON
		-DModule_ITKImageAdaptors:BOOL=ON
		-DModule_ITKImageFunction:BOOL=ON
		-DModule_ITKMesh:BOOL=ON
		-DModule_ITKQuadEdgeMesh:BOOL=ON
		-DModule_ITKSpatialObjects:BOOL=ON
		-DModule_ITKTransform:BOOL=ON
		-DModule_ITKTransformFactory:BOOL=ON
		-DModule_ITKIOTransformBase:BOOL=ON
		-DModule_ITKIOTransformInsightLegacy:BOOL=ON
		-DModule_ITKIOTransformMatlab:BOOL=ON
		-DModule_ITKAnisotropicSmoothing:BOOL=ON
		-DModule_ITKAntiAlias:BOOL=ON
		-DModule_ITKBiasCorrection:BOOL=ON
		-DModule_ITKBinaryMathematicalMorphology:BOOL=ON
		-DModule_ITKColormap:BOOL=ON
		-DModule_ITKConvolution:BOOL=ON
		-DModule_ITKCurvatureFlow:BOOL=ON
		-DModule_ITKDeconvolution:BOOL=ON
		-DModule_ITKDenoising:BOOL=ON
		-DModule_ITK#DiffusionTensorImage:BOOL=ON
		-DModule_ITKDisplacementField:BOOL=ON
		-DModule_ITKDistanceMap:BOOL=ON
		-DModule_ITKFastMarching:BOOL=ON
		-DModule_ITKFFT:BOOL=ON
		-DModule_ITKGPUAnisotropicSmoothing:BOOL=ON
		-DModule_ITKGPUImageFilterBase:BOOL=ON
		-DModule_ITKGPUSmoothing:BOOL=ON
		-DModule_ITKGPUThresholding:BOOL=ON
		-DModule_ITKImageCompare:BOOL=ON
		-DModule_ITKImageCompose:BOOL=ON
		-DModule_ITKImageFeature:BOOL=ON
		-DModule_ITKImageFilterBase:BOOL=ON
		-DModule_ITKImageFusion:BOOL=ON
		-DModule_ITKImageGradient:BOOL=ON
		-DModule_ITKImageGrid:BOOL=ON
		-DModule_ITKImageIntensity:BOOL=ON
		-DModule_ITKImageLabel:BOOL=ON
		-DModule_ITKImageSources:BOOL=ON
		-DModule_ITKImageStatistics:BOOL=ON
		-DModule_ITKLabelMap:BOOL=ON
		-DModule_ITKMathematicalMorphology:BOOL=ON
		-DModule_ITKPath:BOOL=ON
		-DModule_ITKQuadEdgeMeshFiltering:BOOL=ON
		-DModule_ITKSmoothing:BOOL=ON
		-DModule_ITKSpatialFunction:BOOL=ON
		-DModule_ITKThresholding:BOOL=ON
		-DModule_ITKEigen:BOOL=ON
		-DModule_ITK#FEM:BOOL=ON
		-DModule_ITKNarrowBand:BOOL=ON
		-DModule_ITKNeuralNetworks:BOOL=ON
		-DModule_ITKOptimizers:BOOL=ON
		-DModule_ITKOptimizersv4:BOOL=ON
		-DModule_ITKPolynomials:BOOL=ON
		-DModule_ITKStatistics:BOOL=ON
		-DModule_ITKRegistrationCommon:BOOL=ON
		-DModule_ITKGPURegistrationCommon:BOOL=ON
		-DModule_ITKGPUPDEDeformableRegistration:BOOL=ON
		-DModule_ITKMetricsv4:BOOL=ON
		-DModule_ITKPDEDeformableRegistration:BOOL=ON
		-DModule_ITKRegistrationMethodsv4:BOOL=ON
		-DModule_ITKClassifiers:BOOL=ON
		-DModule_ITKConnectedComponents:BOOL=ON
		-DModule_ITKDeformableMesh:BOOL=ON
		-DModule_ITKKLMRegionGrowing:BOOL=ON
		-DModule_ITKLabelVoting:BOOL=ON
		-DModule_ITKLevelSets:BOOL=ON
		-DModule_ITKLevelSetsv4:BOOL=ON
		-DModule_ITKMarkovRandomFieldsClassifiers:BOOL=ON
		-DModule_ITKRegionGrowing:BOOL=ON
		-DModule_ITKSignedDistanceFunction:BOOL=ON
		-DModule_ITKVoronoi:BOOL=ON
		-DModule_ITKWatersheds:BOOL=ON
		-DUSE_FFTW:BOOL=ON
		-DUSE_FFTWF:BOOL=ON
		-DUSE_FFTWD:BOOL=ON
		-DUSE_SYSTEM_FFTW:BOOL=ON
		-DITKGroup_Core:BOOL=ON
		-DBUILD_TESTING:BOOL=OFF
		-DBUILD_EXAMPLES:BOOL=OFF
		-DITK_USE_SYSTEM_EXPAT:BOOL=ON
		-DITK_USE_SYSTEM_ZLIB:BOOL=ON
		-DITK_USE_SYSTEM_TIFF:BOOL=ON
		-DITK_USE_SYSTEM_PNG:BOOL=ON
		-DITK_FORBID_DOWNLOADS:BOOL=ON
		-DITKGroup_Numerics:BOOL=ON
		-DITKGroup_Nonunit:BOOL=ON
		-DITKGroup_Registration:BOOL=ON
		-DITKGroup_Segmentationtion:BOOL=ON
		-DITK_USE_SYSTEM_LIBRARIES:BOOL=ON
		-DITK_USE_SYSTEM_GDCM:BOOL=OFF
		-DITK_USE_SYSTEM_MINC:BOOL=OFF
		-DITK_USE_SYSTEM_VXL:BOOL=OFF
		-DITK_BUILD_SHARED:BOOL=ON
		-DITK_USE_SYSTEM_GOOGLETEST=OFF
		-DITK_INSTALL_LIBRARY_DIR=lib64
		-DCMAKE_INSTALL_PREFIX=/opt
		-Wno-dev
	)


	cmake_src_configure
}


pkg_postinst() {
	mv ${D}/usr/lib ${D}/usr/lib64
}
