# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit java-pkg-2 flag-o-matic desktop autotools

DESCRIPTION="Probabilistic model checker, a tool for formal modelling and analysis of systems"
HOMEPAGE="https://www.prismmodelchecker.org/"
SRC_URI="https://github.com/prismmodelchecker/prism/archive/refs/tags/v${PVR}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL"
KEYWORDS="~amd64 ~x86"
IUSE="test"

SLOT='0'

RDEPEND="virtual/jdk
"
DEPEND="
"

BDEPEND="virtual/jdk
	test? ( dev-lang/python )
"


S="${WORKDIR}/${P}/prism"

#the makefile proposed is tricky. Give it a try...

src_compile(){
replace-flags -O? -O3
emake JAVA_DIR=$(java-config -o) CFLAGS="-fPIC $CFLAGS"
}


src_install(){
mkdir -p ${D}/opt/prism 
cp -r ${WORKDIR}/${P}/* ${D}/opt/prism
}

pkg_postinst(){
cd /opt/prism/prism
./install.sh

ewarn "Consider that /opt/prism/prism/bin is unlikely in your path"
}


src_test(){
make testsfull
}
