# Geospatial Overlay

Here is a collection of ebuild mainly related to scientific softwares for geospatial imageries processing ar data analysis.
Some related dependencies appear; here included old versions for atoms updated in the main gentoo tree (usually not slotted).

This repository is *experimental* and most of this ebuild just (hopefully) work.
Where keywords are (~)arm64, that is because is the only architecture ebuilds were tested on. I would be really surprised by failures on amd64.

If you want to give a try:

	eselect repository add geospatial git https://gitlab.com/FoscoV/geospatial-overlay

	#layman -o https://gitlab.com/FoscoV/geospatial-overlay/-/raw/master/geospatial.xml -f -a geospatial

Merges are wellcommed 
